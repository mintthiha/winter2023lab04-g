public class Toaster {
	private double height;
	private String color;
	private int buttons;
	
	public void printHeight(){
		System.out.println("My height is " + height);
	}
	public void printColor(){
		System.out.println("My color is " + color);
	}
	//setting the attribute height
	public void grow(double height){
		double validInput = validator(height); //deals with validating
		this.height = validInput; //deals with updating the state of this toaster
	}
	//helper function
	//SOLE purpose
	//ensure the input is valid
	public double validator(double input){
		if(input > 0)
		{
			input = input * 1.5;
		}
		return input;
	}
	
	public double getHeight(){
		return this.height;
	}
	//deleted setter method for height
	public String getColor(){
		return this.color;
	}
	
	public void setColor(String color){
		this.color = color;
	}
	
	public int getButtons(){
		return this.buttons;
	}
	
	public void setButtons(int buttons){
		this.buttons = buttons;
	}
	
	public Toaster(double height, String color, int buttons){
		this.height = 10;
		this.color = "Nothing";
		this.buttons = 0;
	}
}