import java.util.Scanner;

public class ApplianceStore {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		Toaster[] toasters = new Toaster[4];
		
		for (int i = 0; i < toasters.length; i++){
			toasters[i] = new Toaster(scan.nextDouble(),scan.next(),scan.nextInt());
		}
		
		System.out.println("BEFORE UPDATE");
		System.out.println(toasters[3].getHeight());
		System.out.println(toasters[3].getColor());
		System.out.println(toasters[3].getButtons());
		
		//toasters[3].setHeight(scan.nextDouble());
			//We have deleted the setter method in a previous question, so we are unable to call it without the program having a compiler error.
		toasters[3].setColor(scan.next());
		toasters[3].setButtons(scan.nextInt());
		
		System.out.println();
		System.out.println("AFTER UPDATE");
		System.out.println(toasters[3].getHeight());
		System.out.println(toasters[3].getColor());
		System.out.println(toasters[3].getButtons());
		
		System.out.println();
		toasters[0].printHeight();
		toasters[0].printColor();

		toasters[1].grow(toasters[1].getHeight());
		System.out.println(toasters[1].getHeight());
	}
}